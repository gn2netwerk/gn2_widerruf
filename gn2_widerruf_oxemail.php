<?php
class GN2_Widerruf_OxEmail extends GN2_Widerruf_OxEmail_parent
{
    protected $_gn2_widerrufattachment = null;

    public function sendOrderEmailToUser($oOrder, $sSubject = null)
    {
        $now = time();
        $showFrom = strtotime(date('2014-06-13 00:00:01'));
        if ($now >= $showFrom) {
            $this->_gn2_widerrufattachment = realpath(dirname(__FILE__)).'/widerrufsformular.pdf';
        }
        return parent::sendOrderEMailToUser($oOrder, $sSubject);
    }

    public function send()
    {
        if (file_exists($this->_gn2_widerrufattachment)) {
            $oxVer = substr(oxConfig::getInstance()->getVersion(), 0, 3);
            $oxVer = intval(str_replace('.', '', $oxVer));

            if ($oxVer == '44') {
                $this->addAttachment(dirname($this->_gn2_widerrufattachment).'/', basename($this->_gn2_widerrufattachment));
            } else {
                $this->addAttachment($this->_gn2_widerrufattachment, basename($this->_gn2_widerrufattachment));
            }
        }
        return parent::send();
    }
}
?>