<?php

class GN2_Widerruf_OxContent extends GN2_Widerruf_OxContent_parent
{
    public function loadByIdent($sLoadId, $mod=true)
    {
        $now = time();
        $showFrom = strtotime(date('2014-06-13 00:00:01'));
        if ($now >= $showFrom && $mod) {
            $modded = false;
            $orgId = $sLoadId;
            $setOffline = false;

            if ($sLoadId == 'oxagb') {
                $sLoadId = 'oxagb20140613';
                $modded = true;
                $setOffline = 'oxagb';

            } else if ($sLoadId == 'oxrightofwithdrawal') {
                $sLoadId = 'oxrightofwithdrawal20140613';
                $modded = true;
                $setOffline = 'oxrightofwithdrawal';

            } else if ($sLoadId == 'oxuserorderemailend') {
                $sLoadId = 'oxuserorderemailend20140613';
                $modded = true;
                $setOffline = 'oxuserorderemailend';

            } else if ($sLoadId == 'oxuserorderemailendplain') {
                $sLoadId = 'oxuserorderemailendplain20140613';
                $modded = true;
                $setOffline = 'oxuserorderemailendplain';

            }

            // ALTE CMS-SEITEN OFFLINE SCHALTEN
            if ($setOffline != "") {
                $query = "
                    UPDATE `oxcontents`
                    SET `OXACTIVE` = '0', `OXACTIVE_1` = '0', `OXACTIVE_2` = '0', `OXACTIVE_3` = '0'
                    WHERE `OXLOADID` = '".$setOffline."';
                ";
                oxDb::getDb()->Execute($query);
            }

        }

        $response = parent::loadByIdent($sLoadId);

        if ($modded && empty($response)) {
            $this->_gn2_widerruf_install($orgId, $sLoadId);
            return parent::loadByIdent($sLoadId);
        }

        return $response;
    }

    protected function _gn2_widerruf_install($orgId, $sLoadId)
    {
        $page = oxNew('oxcontent');
        $page->loadByIdent($orgId, false);
        $newPage = clone($page);
        $utils = oxNew('oxutilsobject');
        $uid = $utils->generateUID();
        $newPage->setId($uid);
        $newPage->oxcontents__oxloadid->rawValue = $sLoadId;
        $newPage->save();
    }
}

?>