<?php

/**
 * Module information
 */

 $aModule = array(
    'id'            => 'gn2_widerruf',
    'title'         => 'gn2 :: Widerruf',
    'description'   => '',
    'thumbnail'     => 'gn2_netwerk.jpg',
    'version'       => '1.0',
    'author'        => 'GN2 netwerk',
    'extend'        => array(
        'oxcontent' => 'gn2_widerruf/gn2_widerruf_oxcontent',
        'oxemail'   => 'gn2_widerruf/gn2_widerruf_oxemail',
    )
);