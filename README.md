# gn2 :: Widerruf  

*Für die OXID-Versionen 4.4.x bis 4.8.x*

Am 13.06.2014 treten in Deutschland einige Änderungen im Widerrufsrecht nach der europäischen Verbraucherrechterichtlinie (VRRL) in Kraft. Um Ihren Onlineshop auf die neuen Regelungen vorzubereiten sind einige Text- und Moduländerungen notwendig.

Dieses Modul dupliziert Ihre OXID AGB- und Widerrufsseiten `oxagb ==> oxagb_20140613` und `oxrightofwithdrawal ==> oxrightofwithdrawal_20140613`. Diese neuen CMS-Seiten können schon vor dem 13.06.2014 den neuen Regelungen entsprechend angepasst werden. Ab dem 13.06.2014 werden automatisch die neuen Texte verwendet.

Zusätzlich werden alle Bestellmails ab dem 13.06 automatisch um ein Muster-Widerrufsformular als PDF-Anhang erweitert.


## Installation

1. Laden Sie das Modul als `gn2_widerruf` in den Ordner `/modules` hoch.
2. gn2_widerruf aktivieren (OXID 4.6+: Extensions -> Module -> gn2 :: Widerruf -> aktivieren). OXID 4.4-4.5 erfordern manuelle Moduleinträge: ![OXID 4.4 und 4.5 Moduleinträge](https://bytebucket.org/gn2netwerk/gn2_widerruf/raw/44d74dd98c5b651197ab02431688f19ca3f297ff/448.png?token=7a4b55abf9334376e8a38e6b2d290e5ce0e90622)
3. Passen Sie nun die neuen AGB + Widerrufsseiten entsprechend an. ![CMS-Pages](https://bytebucket.org/gn2netwerk/gn2_widerruf/raw/44d74dd98c5b651197ab02431688f19ca3f297ff/cms.png?token=4a16f17a1017420615bcae93abe50c0048977dea) (Einige Hinweise finden Sie unter [http://www.trustedshops.de/shop-info/wp-content/uploads/sites/3/2014/03/Muster_Widerrufsbelehrung_Waren.pdf](http://www.trustedshops.de/shop-info/wp-content/uploads/sites/3/2014/03/Muster_Widerrufsbelehrung_Waren.pdf)).
4. Fertig.
